《起始的足跡》，氏族建築的最頂層。在氏族成員中只允許事務員和氏族Master進入的氏族Master室，我笑眯眯地低頭看著桌上。

在桌子上面有五具五公分左右的小小人型，正進行著賽跑。

人型十分精緻，就像是把人類原封不動地縮小一樣。
各自的裝扮分別是，魔導師、僧侶、劍士、盜賊和錬金術師，雖然長袍和鎧甲的細節馬馬虎虎，但還是想稱讚一下自己幹得不錯。

一邊擦拭發熱的手鐲，一邊嘗試讓人型們在桌子邊緣漫步。
它們並不是具有實體的玩偶，而是使用才得到的寶具──『舞動光影』生成的幻像。桌上顯示的轉角線和小山也都是幻像。
雖然起初無法很好地設定形狀和顏色，但通過最近幾天的練習，我成功地塑造出還算精緻的人型幻像。

原本，寶具自身就具有一定程度上補全圖像的功能。
但是即便如此，練習取得成效還是非常令人開心。看起來也很歡快，且不論是否有用──若是用於玩樂，比它更有趣的寶具應該是寥寥無幾。

用食指戳向幻像之一的同時，讓其做出嚇癱的動作。其他的幻像則是做出抗議的動作。儘管全都是自己在操縱，卻像在使役精靈小人一樣。微笑停不下來。

雖然想向別人展示，但如果被知道我在玩這種玩偶，冷酷的形象就會破滅，所以很遺憾做不到。

接下來，我又試著造出小到可以放在手上的龍。而且不止一頭，而是兩頭，三頭，四頭地不斷增加。顏色當然是各不相同。
當我還跟著隊伍時，數次與龍遭遇過。雖然細節有些模糊，但要製作大致的幻像也不成問題。

龍在我的頭頂扇動翅膀盤旋著。我集中精神，試著摸索更具真實性的飛行方式。

這件寶具唯一的缺點就是它的範圍只有一米二十公分。如果再擴大一些，遊戲範圍也會變得更大。突然好奇是否存在有效範圍寬廣的上位替代。

因為是幻像，所以不受物理屏障的影響。我操縱龍穿過窗戶飛到外面，超過範圍後便消失在空氣中。

「真是軟弱的龍啊」

嘛～，雖然不是龍的錯。
當我正玩著飛龍遊戲時，氏族Master室的門突然打開。身體不由得嚇了一跳。來者是伊娃。
雖然我沒有該做的工作，但幼稚的遊戲被看到的話還是會尷尬。我急忙消去放飛起來的龍。

或許是沒來得及，在眼鏡中，伊娃的眼睛睜得渾圓。

「！？？？？？什，什麼啊，剛才的？」

「⋯⋯什麼都沒有喲。只是你突然進來嚇到我了」

雖然我早已習慣莉茲的突然闖入，但伊娃不敲門可是相當罕見。
聽到我的話，伊娃不可思議似地眨了眨眼。

「誒⋯⋯？可是我，敲過門了」

⋯⋯忙著飛龍，完全沒有注意到。果然這種事不應該在這個房間做啊。

也不用問是什麼事。在走過來的伊娃手裡，拿著多到令人厭煩的信件。
原本《嘆息的亡靈》與其他的高等級隊伍相比，來自貴族的聯繫就很少，但自從在拍賣會上與格拉迪斯卿扯上關係後，寄來的信件數量不知為何不斷增加。
而且十分奇怪的是，周圍似乎都在傳我救了艾克蕾爾小姐。救她的不是我而是亞克吧⋯⋯⋯

若是抱有揚名立萬的野心，或是尋求門路的獵人，應該會心懷感激地收下吧。但不湊巧，我可是一個想引退的獵人。只會鄭重地謝絕。
如果情況真的不妙，也考慮過從澤布魯迪亞逃亡。

當我坐回椅子上，並在平復呼吸時，突然發現伊娃的視線正投向桌子。更具體地說──是投向忘記消去的幻像小人。
伊娃抬起臉，然後看向我。她的眼神就像是在懷疑自己的理智一樣。

小人手足無措地在桌子上亂跑，接著從桌子上跳到我這邊。我輕輕乾咳一聲，在椅子上翹起腿並向後仰。

「⋯⋯那麼，有什麼事？」

「？？？難道是以為這樣就可以蒙混過關？剛才的是什麼！？」

伊娃繞到後面並探頭看向桌子下面，但小人早已消失。不可能找得到。
我決定以冷酷為目標的同時，順帶也以神秘為目標。我交叉起雙手，露出冷酷的笑容。

「呼⋯⋯我也是有秘密的喲」

「這個⋯⋯⋯⋯我當然知道⋯⋯」

伊娃再三歪起頭，但還是用力地點了點頭，仿彿在強迫自己接受一樣。
似乎是振作了起來，伊娃乾咳一聲。

「這次來自貴族的信件好像略多。可以的話請你過目一下──」

在桌子上面，剛才還不存在的與我一模一樣的迷你模型把雙手舉到頭上準備接住信件，看到這些，伊娃的表情僵住，緩緩看向我這邊。

我點頭示意後，她就小心翼翼地把信件放到那雙手上──信件的重量壓扁了迷你克萊伊。
伊娃的臉色刷地變白，並急忙拿起信件，不過信件下面當然什麼也沒留下。迷你克萊伊就像幻像一樣消失了。
與其說就像幻像一樣⋯⋯不如說就是幻像。

「誒？？那個？我──」

「啊啊，不用擔心喲。話說，是什麼事來著？」

對於罕見地驚慌失措的伊娃，我回以一個溫和的笑容。這個，非常地有趣⋯⋯⋯

§ § §

『澤布魯迪亞西南貧民區』。正常人絕不會踏入的陰暗小巷的一角，莉茲・斯馬特不耐煩地咂了砸嘴。

「⋯⋯啊～，哈。魔術結社就是這樣才令人討厭。都不敢正面對抗⋯⋯」

「畢竟殘存的非法魔法結社基本都很謹慎⋯⋯不過我認為阿加莎之塔在其中也尤為棘手⋯⋯」

做出回應的是，兜帽深深戴至眼睛，盡量遮住臉的西特莉。

以在拍賣會上相互競爭過作為情報依據，莉茲她們正在搜尋阿加莎的踪跡。
調查拍賣會的競拍對手，接著調查參展者，最後是拍下『阿加莎』的獵人──格雷格，從與其接觸的人進行打探。

莉茲她們，《嘆息的亡靈》早已習慣以犯罪集團為對手。被犯罪者隊伍糾纏是家常便飯，搗毀犯罪組織和魔術結社也不是第一次。
但是，這次的對手與莉茲她們以往面對的小人物級別不同。

在莉茲的面前，三個魁梧的男人被迫跪著。
在實踐中鍛鍊出來的肉體纏繞著一股，只有吸收大量瑪那源的人才會放出的威懾感，從久經使用的鎧甲上，其實力也可見一斑。扔到路邊的塗成黑色的短劍、刀和法杖都是高級品，如果要買新品，價格會足足超過一千萬吉爾吧。

帝都是獵人的聖地。然而，有光明的地方就會有黑暗。
男人們是莉茲循著蛛絲馬跡找到的沒落獵人的辦事通。承接包含暴力的違法工作並暗中活動，是擅長對人戰的黑惡獵人。其實力遠超獵人的平均水平，經常給帝都的探險者協會帶來麻煩。
不過，讓莉茲來說的話，那些男人只是落伍者而已。是忘掉獵人的本分，逃避遍佈魔物、幻影和凶惡陷阱的寶物殿，選擇壓榨弱者的喪家犬。
也並不是在否定壓榨弱者這種事，而是那種平日只以弱者為對手的失敗者，沒有理由會令人害怕。

被迫跪著的男人們，雙臂被綁在背後，頭部被紙袋罩著。雖然看不見表情，但他們的身體明顯地因緊張而顫抖著，混雜了汗水和血液的惡臭彌漫在空氣流通差的狹窄通道裡。

儘管對方是時刻保持警戒的多人團體，是貨真價實的職業辦事通，但擊敗卻相當簡單。搜尋都要比這困難。

然而，費盡精力地找到居所，為留活口而小心又小心地制伏並拘束，然後進行審問，得到的情報卻和莉茲期待的不同。

與委託人相關的情報完全沒有。
委託方式是信件，報酬是預付。假如奪取到東西，委託人應該會現身吧，但現在切換作戰計劃也為時已晚。

看起來也不像是在撒謊。
承受高等級獵人莉茲的殺意，能保持沉默的人並不多，而且為了讓他們招供，西特莉還注射過違禁藥水。名字、家庭成員、經歷等個人情報不斷地吐露出來，也看不出他們具有耐性。

由於最近幾天的搜尋徒勞無功，莉茲徹底變得失落，並對西特莉發起牢騷。

「我說你啊，明明曾是成員之一，什麼都不知道是怎麼回事？」

「因為完全實行分工，我又打算再待一段時間⋯⋯」

聽到莉茲的話，西特莉在兜帽之下露出略顯無奈的神情。

名為『阿加莎之塔』的魔術結社奉行秘密主義。
被驅逐的優秀魔導師都在按照各自的理論進行著研究，每個研究室的同伴既沒有交流也得不到詳細的情報。協調工作分配有專門的人員，成員所知道的情報基本只有所屬研究室內部的事情。
儘管作為諾特・科庫雷亞的頭號弟子參與過好幾個研究，西特莉卻沒有走出過諾特的研究室。雖然打算不久後把活動範圍擴大到外部，但是由於諾特・科庫雷亞的研究在『阿加莎之塔』獲得很高的評價，所以設備和預算都很充足，尋找新研究室的必要性也就很小。假如克萊伊沒有下達歸還命令，現在應該還在高興地從事著那個研究吧。

但是，既然事情會變成這樣，即使慢慢來也應該先收集情報。若是擔任室長的諾特・科庫雷亞，就應該還知道些什麼，可是那位原大賢者已經失去記憶，現在則被關押在大監獄裡。自己毫無辦法。

對手是作為世界之敵長年立於頂端的巨大組織。無論莉茲和西特莉有多麼強大，要與之抗衡就需要與力量不同的影響力。
已經滲透到澤布魯迪亞的上層是事實，但就算辛苦調查進行聲討也無濟於事吧。要是事態發展到那步，法律恐怕不會站在莉茲她們這邊。

雖然並不會害怕險境，但也沒有那麼感興趣。
莉茲像貓一樣大打呵欠，然後毫不在乎地直言道。

「已經厭倦了。放棄吧？又浪費時間，格雷格也安全了，大概。我可沒有閑情搭理弱雞呢。而且托克萊伊醬的福得到了哥雷姆，剩下的怎樣都好」

「姐姐⋯⋯真是的，總是動不動就厭倦！」

「如果想要新的材料，既然抓到三個人，使用他們不就行啦？」

莉茲用下巴示意早已喪失抵抗氣力的三個俘虜。西特莉就像生氣一樣反駁道。

「要怎麼搬運！？如果從這裡搬到研究室，會引人注目的吧？說到底，下一個檢體我想要的是魔導師類型──」

「不知道。在這附近抓如何？」

「怎麼這樣──姐姐也是，知道隊伍的規則的吧！？」

《嘆息的亡靈》的規則有三條。

大家友好相處。
禁止對一般人出手。
民主主義。意見相左的話就用多數表決來決定（順帶一提，隊長擁有五票）

雖然有點過於慎重，但西特莉認為是合理的規則。
而只要第二條規則還存在，西特莉就無法對一般人出手。即使在加入阿加莎之塔期間，對於從貧民區虜來做人體實驗的一般人，西特莉也從未直接出手過。因為這個原因，還被原師父諾特評價說『還很天真啊』，不過這對西特莉來說是無可奈何的事。

貧民區居民的渾濁眼睛，從小巷的外部，從建築物的窗戶，觀察著莉茲她們引起的騷動的結局。
面對焦急的妹妹，莉茲像是想到好主意似地，笑著拍了一下雙手。

「⋯⋯好，決定了！雖然有點不甘心，去拜託克萊伊醬吧？」

「⋯⋯」

「沒問題，克萊伊醬很溫柔，而且這次的事肯定也預料到啦。西特要是怕的話，就讓我來道歉並幫你請求！如此一來不用浪費時間就能解決，又能有時間與克萊伊醬約會，還能有時間鍛鍊小緹，是個好主意對吧？決定了！」

趁著西特莉什麼都沒說，莉茲自己迅速地得出結論，然後自信滿滿滴交叉起雙臂。

一邊看著她的笑容一邊思考起來。
說實話──想盡量避免給隊長添麻煩。但是，因為要花費時間，所以最終還是有可能會添麻煩，而且自從成為獵人也還是一直在添麻煩。從很久以前就總是與其商量，事到如今，也不是害怕帶去一件麻煩事的交情。

苦思了好一會兒，卻沒有想出好主意。最終西特莉得出的結論也是一樣。
這也是兩個人的意見不一致時，基本會抵達的結果。

從妹妹的表情領會到結論，莉茲用力伸了伸懶腰，然後指著三個俘虜。

「那麼，這些傢伙怎麼辦？」

「唔～嗯，又不能帶回去⋯⋯」

聽到莉茲的問題，西特莉再次低頭看著三個俘虜。
雖然手臂被綁住，還被注射吐真劑，並且滿身瘡痍，但如果再等一段時間，心靈姑且不論，身體應該會恢復吧。

說實話怎樣都好。研究室又遠，帶回去的風險又太高。雖然招到了怨恨，但那對《嘆息的亡靈》來說，早就習以為常。
把手指搭在嘴唇上，西特莉眨了眨眼睛。

「殺掉不管的話，這裡的居民會蜂擁而至，我想明天連骨頭渣都不會剩下⋯⋯」

「哼～嗯。就那麼辦吧？」

就像是定下今天的晚飯一樣的平淡語調。

不知從哪裡傳來咔嗒咔嗒的聲音。
是紙袋的裡面。牙齒咬合的聲音。
之所以沒有說出話來，是因為沒有那麼做的氣力吧。但或許是能聽到對話，他們的身體不由自主地顫抖著。
從宛如進行日常對話般的平淡語調中感受到了真心。
這兩個人對他們的性命毫不在乎。是沒有殺意就能弄髒雙手的人。

跪著的三人的身體顫動搖晃起來。就在此時，西特莉突然想到一個好主意，並發出明朗的聲音。

「啊，但是等一下，姐姐。收作手下比殺掉可能會更好！我正好想要習慣弄髒雙手的部下，可做成吉魯吉魯君又太粗劣，不覺得再利用比扔掉更好嗎？」

「誒～？手下？可是我才不要軟弱的手下？」

「那麼我就三個人一起收下啦！啊，但是⋯⋯聽一下本人的想法，不行的話只有處理掉⋯⋯但是，克萊伊桑似乎不太喜歡殺人⋯⋯」

西特莉繞到跪著的三人前面，輕觸他們裸露出來的脖子。從紙袋裡傳出喘不上氣的聲音。與吉魯吉魯君戴的不同，頭上有些髒的紙袋並未在眼部開孔，什麼都看不出來。

平復呼吸後，西特莉溫柔地問道。莉茲則從她身後摻和進來。

「那個，各位。繼續做辦事通也沒關係⋯⋯要不要做我的部下？當然不會強求。如果有意願的話」

「誒～，不要對吧？西特的部下什麼的。人生的前方可是一片黑暗──不如說死掉會更好啊⋯⋯對吧？喂，說句話啊。你們這群廢物！」