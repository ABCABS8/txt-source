１６

「諾瑪殿。要鄭重說的話是什麼？」
「領主大人。我和艾達在看病時，札克先生一直都在無意識地說夢話」
「啊啊，好像是呢」
「我有寫下那些夢話。也有叫艾達寫下」
「喔？」
「雖然只集合了片段話語，但能發現到相當不得了的東西。儘管如此，由於話語很難聽清楚，意義不明的部分也多，又是毫無脈絡的夢話，所以沒辦法從中好好取出情報。就算想聽取，也可能有聽錯的吧。說不定也混有並非事實的臆想和幻覺」
「還真煞有其事呢。那傢伙究竟說了什麼？」
「現在就開始呈報那情報的斷片。艾達」
「是？」
「我說的話，如果跟你聽取的內容對不上，就告訴我吧」
「嗯。知道了」


１７

「首先，札克先生，不，寨卡茲家恐怕，知道地龍托隆在哪裡」
「什麼！」
「也有說過祖父發現了托隆這種話，所以發現的人說不定是寨卡茲家兩代前的當主。應該是在五十年前左右判明的。說到托隆這名稱能想到的，就是地龍托隆」
「怎麼可能。竟然是地龍托隆」
「然後，我認為札克先生建立了討伐地龍托隆的計畫。不，準備了三代也說不定」
「討伐地龍托隆嗎」
「札克先生似乎造了龍滅劍這樣的東西。是消滅龍的劍的意思吧。恐怕花了好幾年」
「妳說龍滅劍？呼嗯。會調查看看的。然後呢？」
「然而托隆下落不明了」
「喔？」
「札克先生發狂似地追尋托隆的去向。似乎派出了幾個調查團」
「聽說龍這種東西，在一個場所定居的話就不會離去。是發生了什麼事嗎」
「我不知道。總之派遣了好幾個調查團。大概花了好幾年調查吧」
「然後怎麼樣了」
「忘記說了，地龍托隆似乎是在大森林裡」
「呼嗯？從柯古陸斯到大森林的距離，還挺長的。嘛算了。然後怎麼樣了？」
「雖然不清楚跟托隆的事有什麼關聯，但札克先生應該在大森林裡發現了古代神殿之類的東西」
「古代神殿？」
「是的。那說不定有別於托隆的事，是寨卡茲家從很久以前就擁有的知識」
「那個，我覺得不對」
「艾達。有什麼不對嗎」
「說了，是老夫找到的。所以，我覺得是札克先生找到的」
「啊，的確。那個老夫找到的是指古代神殿啊。這樣啊。想必就是這樣。謝謝，艾達」
「誒嘿嘿嘿嘿」
「那麼，那個古代神殿怎麼了」
「似乎有很不得了的財寶。札克先生一個不留全收歸己有了。應該是在十七年前。施療師亞特露娜明確地把握了，詛咒的起點是在十七年前。也就是說，亞特露娜殿認為，是古代神殿的詛咒在侵蝕札克大人吧」
「這還真是。是嗎。原來是這樣啊」
「怎麼了嗎，領主大人」
「是個謎團啊。那傢伙太富有了。在各處幹了各種事，明明有時也遭遇了失敗或吃了苦頭才對，但活動別說變小了，規模反而越來越大。那些錢究竟是在哪裡，從以前就覺得很不可思議」
「那是，從很久以前就有的嗎？」
「不。是在這十幾年」
「原來如此」
「妳說的情報，就這些嗎？」
「算是吧。另外，硬要說的話，我剛剛說的，是札克先生在意到會不斷說出夢話的。被記憶苛責了呢」
「喔。真讓人感興趣。但是，竟然是地龍托隆嗎。難道說，札克那傢伙真的在考慮叛離扎卡王国嗎？」
「那是什麼意思？」
「哈哈哈，就連博學多聞的諾瑪殿也不知道嗎。地龍托隆被稱為〈豐收之龍〉，據說托隆棲息的地域會很繁榮」
「是。那別名我知道」
「那麼，知道這扎卡王国，是奠定在地龍托隆的屍體上的嗎？」
「誒」
「建国王打倒托隆的場所，據說就是現在的王都芬凱爾」
「原來是這樣啊」
「瑪爾王国原本也棲息了地龍托隆的樣子」
「札克先生是認為，打倒托龍就能建立王国嗎？」
「有點不同。打倒托隆，入手了那魔石的話，就能得到從扎卡王国獨立的大義名分。展示托隆的屍體的話，在民眾之間獨立的機運就會增加吧」
「啊啊，原來如此」
「柯古陸斯本來就是跟扎卡王国無關的獨立領。被編入扎卡王国的時期很晚。這附近的領主都知道，那邊討厭王都的干涉。但是，是嗎。托隆嗎。原來如此」
「那個，可以說個話嗎？」
「當然。怎麼了？」
「要建立国家，就一定要托隆嗎。確實也有其他知道所在地的龍吧？」
「啊啊。王龍阿特拉西亞和飛龍盧德在本国裡，所在地很清楚。人類靠近不了就是了」
「那個龍不行嗎？」
「在六種特殊的龍之中，不知為何，只有托隆被視為建国的象徵。不過也不是所有国家都是在打倒托隆後建立的才對」
「六種特殊的龍，嗎？」
「地龍奧格托。地龍伊瑪目。地龍托隆。飛龍猶固。飛龍柯丹。飛龍盧德。據說這六種龍，在一個時代只會出現一隻個體。這間館的入口有裝飾繪畫喔」
「誒？是那樣嗎」
「哈哈哈。明明來了這麼多次也沒注意到嗎？」
「阿勒？剛剛，不是有說名叫阿特什麼的龍嗎？」
「王龍阿特拉西亞嗎。這不是龍種特殊個體，是神獸」
「神售？」
「神之獸的意思。雖然不知道是真是假，但神獸好像會說人的語言喔」
「誒誒誒？明明是獸？」
「哈哈。也不知道到底是怎樣。就算是王龍阿特拉西亞，雖然有時會從遠方目擊到巨大的身姿，所以確實是存在的，但也不覺得真的能說人的語言」
「龍阿。很強吧」
「有傳承說，在扎卡王国建立以前的某国的軍隊，被地龍托隆毀滅了。要是札克跟托隆戰鬥的話，柯古陸斯現在可能已經被消滅了呢」
「雷肯的話就能贏」

艾達說了沒有根據的話。

「哇哈哈哈哈。是阿。如果有一百個雷肯的話說不定能贏。哈哈哈」
「領主大人」

諾瑪以跟剛才稍微不同的聲響說道。

「嗯？」
「這次，領主大人協助了札克先生的治療。我很感激領主大人的肚量之大」
「對阿！明明其實是敵人，但是在施術成功的時候拍手了。那好帥阿」
「老夫自己也對那感到不可思議。但是在得知施術成功時，老夫很感動。現在一想，這次明明是殺掉那傢伙的千載難逢的機會。不，在這城鎮殺掉的話會很糟糕，但要是在回程時派出刺客就好了。老夫到底在幹什麼」
「我覺得沒辦法」
「什麼？什麼意思？」
「那八位護衛，非常強」
「是有覺得應該很有本事」
「我不論跟那之中的誰打，都會被殺掉」
「妳說什麼？身為尼納耶迷宮踏破者的妳？」
「那樣的人一直都兩人一組護衛喔。我怕得不得了呢」
「也就是說，老夫得為自己沒被殺掉感到高興嗎」
「說得是呢」