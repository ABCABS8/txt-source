「沒錯，是魔法。麻里子你昨天，能夠普通的使用點火與燈的吧？」
「誒都，是的」

在旅館中所度過的一天時間中，麻里子已經看過並已經自己使用過了幾個種類的魔法。全部都是遊戲中沒有，是日常生活中使用的魔法。

「如果那一部分能夠使用的話，那在狩獵時候要用到的魔法想來不也是能夠使用的嗎。就在打算來確認的時候，正好看到了米蘭達你們正在練習。想著那不正好麼就出來了」
「說道狩獵的時候使用，是攻擊系的魔法嗎？」
「當然是這樣啊」

攻擊系魔法 ──顧名思義即是攻擊他人的魔法 ──是在遊戲中與恢復系魔法並列的最受歡迎的魔法。雖然其中大部分是只能在戰鬥中使用的東西，但用魔法來攻擊可是幻想系RPG的精華所在。具備充分等級以及智力的角色所放出的攻擊魔法的威力，就算與近距離用武器造成的傷害相比也毫不遜色。

「噢噢，要是麻里子殿的話那樣的魔法使用起來也不會很困難的吧」
「哦呀，米蘭達也那樣想嗎？」
「就是這樣，塔莉婭大人。擁有如此劍技的麻里子殿，很難想像會完全沒有魔法相關的知識和技能」
「啊啊，我也這麼認為的，只要看過實物的話就會想起來（⋯⋯）」

話說道了這裡，就連麻里子也注意到了。塔莉婭打算來教導關於這個世界的魔法。

「老板娘桑，特意過來真的非常感謝」
「要道謝還早呢。即使是我也不是什麼都會使用。像我這樣的頭髮，通常會比較擅長火系的魔法，但除此之外的就。啊啊，麻里子。你知道魔法是有系統的麼？」

塔莉婭捏起了稍許混著有白髮的頭髮，好像突然想起了什麼。

「誒都，神會影響到頭髮所呈現出的顏色的話，昨夜已經問過薩妮婭桑了。但是，說到系統⋯⋯⋯啊啊，頭髮的顏色，是與神相對應的呢」
「啊，是那樣。還記著嗎。魔法中能有風、火、木、水、金、土、生命這七個系統，是分別受到了七柱之神的影響。也因此自己會擅長自己顏色的魔法。只不過，中級以上的魔法也有是由２個以上的系統所組成，所以全部的系統也不是僅僅分為七類」
「中級以上的話⋯⋯」
「與其說是威力，不如說是其本身的上手難度。魔法根據那根被分為下級、中級、上級。用火魔法來說的話、不能在某種程度上將下級的火矢那種魔法運用自如的話就無法掌握中級的火球」
「啊，原來如此」

（火矢和火球都是遊戲中的魔法，雖然不是最高級的等級，但我也能使用。果然這麼考慮的話這裡果然是⋯⋯）

遊戲中也有下級，中級，上級這個區分。區分的辦法也大體上一樣。譬如，中級魔法在其獲取條件和獲取任務發生條件中，幾乎都有「下級魔術ＸＸ多少等級以上」存在。

「那麼，就走吧」
「誒？　是要去哪」
「哪裡，要說魔法射擊的地方不就一定是在那裡麼」

塔莉婭用手指向靠著牆立在那的靶子，就這樣朝那邊走去。

「要說明的只有這些嗎」
「你在說什麼呢。不實際去試試，怎麼會知道你能不能使用」
「就是那樣。走吧，麻里子殿。我也想看看麻里子殿的魔法」

米蘭達也收起木刀跟上了塔莉婭。麻里子驚慌的追上那二人。


◇

在運動場中央正中入神交流的三人正對面的。是靠著牆壁立在那，於樁的前面粘有畫著同心圓的木板的質樸的靶子。那三件分別等間距的排列著。

「從這裡到那邊，大概有三十米左右。這就是火矢經常會使用到的距離」
「再遠的話就夠不著了嗎？」
「不對。就和燈一樣，要是增加注入的魔力也可以到達更遠的地方。只是距離太遠對移動中的東西很難命中，再加上因為一次注入的魔力增多，可以使用的次數也會減少。最好就是在這種距離來使用」

雖然是正確的答案，不過在麻里子心中卻感到了疑問。在遊戲中攻擊魔法的射程是被決定好的，無法通過使用者的意志來提升等級增加射程。

「暫且，先試著射一發。看好」

塔莉婭轉向靶子的方向，以左邊的靶子為目標舉起右手抬起手掌。

「火矢」

伴隨著塔莉婭平穩的聲音共同放出的光芒，如鉛筆大小的火之矢發出了嗖的一聲從她的手心中飛奔而出。就像是十字弓的矢一樣，朝著一條線筆直的前進。

刹那間，隨著嘣地一聲劇響板子裂成兩半掉落到地上。在靶子的表面有著明顯的燒焦痕跡，煙正從上面微微升起。

「這就是下級魔法、火矢」

塔莉婭回頭看先正在後面看著的麻里子她們。

「麻里子，你也做一次吧。我想應該是可以做到的」
「明白了。我試試看」

麻里子點著頭來到塔莉婭的右側。在遊戲中能夠普通的使用魔法。不覺得麻里子自身會無法使用。

「另外，姿勢什麼的不是一定的。只要用容易射擊的姿勢就好」
「是」

那樣回答道，麻里子對好正中間的靶子像塔莉婭一樣舉起手。在二人後面，米蘭達將手握在胸前眼睛閃閃發亮。

「那麼，射擊。火矢」

和使用燈、水的時候一樣，但遠遠在那之上耗費了更多的魔力，能感到力量從麻里子右手中奔馳而出。

光炸裂開來。