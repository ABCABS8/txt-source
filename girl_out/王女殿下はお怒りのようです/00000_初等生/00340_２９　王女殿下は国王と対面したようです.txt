公爵邸的客廳裡，斯卡洛和迪安娜以及長子弗里德和次子奇裡阿斯聚集在了一起。

「那個禁忌之子，引起了這麼不得了的騷動啊！」

斯卡洛在桌子上狠狠地錘下了拳頭。桌子上的茶杯掉在地板上摔碎了，但是誰都沒有餘裕去在意這些。

「這樣一來，我們家在社交界的評價就驟然下降了。該怎麼做⋯！」

聽了斯卡洛的話後，迪安娜低下了頭，緊緊地握著扇子。

「弗里德，長女薩利娜還沒回來嗎？」
「已經派人過去了，不過，薩利娜在婚約者的領地裡。通知送達還需要一段時間。」
「離薩利尼亞結婚就剩一周的時間⋯⋯那個瘟神！」

揮舞著拳頭臉色漲紅的斯卡洛，吐著顫抖的氣息，垂頭喪氣著。

「奇裡阿斯，克莉絲塔怎麼樣了？」
「克莉斯姐姐還在睡覺⋯⋯⋯我從昨天開始一直在看護她⋯⋯」
「這樣啊，不好意思，今後也拜託你照看她了。」
「好的。」

客廳裡變得沉寂。在萬籟俱寂的房間中混雜著各種各樣的感情。恐怖、憤怒、達觀、悲傷、憎恨，還有嫉妒。

「如果知道會變成這樣的話，那個禁忌之子⋯⋯」

令人不快的，但卻感覺不到霸氣的軟弱黛安娜的自言自語靜靜地落在了房間裡。
路維克和妮可兒悄悄地打開點了門縫，偷聽到了公爵家的會話。

（這個家，有資格說這話嗎）

路維克垂下了眼睛。多蘿瑟露有著才能和美貌，那是誰都沒有的東西。但是相對的，親人的愛、家族的溫暖，這種誰都理所應當得到的東西，她卻沒有。造成這種情況的毫無疑問是菲利亞里吉斯公爵家，在這個房子裡所有的人。

（（大小姐））

心緒已經飛到了在學院的多蘿瑟露那兒，兩人只顧擔心自己的主人。
清晨的盧克勒齊亞學園一片騷動。羅修福德和他的隨從們引起了精靈的憤怒，「冰之惡魔」多蘿瑟露・諾亞・菲利亞里吉斯用魔法鎮壓了這一切，這事在學園中廣為流傳。

（那麼，今天該怎麼過呢？）

在那情形下，只有本人沒有動搖，向周圍的人綻放著自己的光芒，忽視著周圍的視線。像往常一樣在玄關大廳思考著今天日程的蕾蒂雪爾，到校的學生們用混雜了好奇心、恐懼和敬畏的眼神遠遠地看著她。

「多蘿瑟露大小姐。」

在這一群遠遠地圍觀著她的學生們之中，有人向她打了招呼。蕾蒂雪爾回頭一看，米蘭達蕾特帶著擔心的表情跑了過來，後面還有米蘭達蕾特的未婚夫希爾梅斯，以及吉克的身影。

「啊，大家們，早上好。」
「比起那個，你昨天沒事吧？雖然我不在現場，但大家都說『很不妙』。」

對著草草打著招呼只顧關心蕾蒂雪爾的米蘭達蕾特，蕾蒂雪爾輕輕地搖了搖頭。

「沒事，感謝你的關心，米蘭。」
「真的？我聽說昨天有很多人身體不適？」
「那都靠吉克幫忙照看了。」
「這樣啊，太好了。」

米蘭達蕾特鬆了一口氣。希爾梅斯對旁邊心愛的婚約者說道。

「你看，蕾蕾，我不是說了不用擔心多蘿瑟露大人嗎？」
「是的啊，利夫偶爾也會說些好話啊。」
「偶爾是多餘的！」

輕輕地把臉轉向一邊的米蘭達蕾特和毫不退縮的希爾梅斯，蕾蒂雪爾微笑地凝視著他們那個樣子，這種傲嬌的關係還是老樣子啊。

「那麼我就去上課了，多蘿瑟露大人請加油啊。」
「嗯，慢走啊，米蘭。」
「多蘿瑟露大人，昨天沒比成，今天放課後請增加劍術的訓練。」
「好好，我知道了。」

米蘭達蕾特和希爾梅斯的身影消失在走廊的對面。蕾蒂雪爾目送著她們，微微地嘆了一口氣。

「我知道你們在，請出來吧。」

蕾蒂雪爾向著什麼都沒有的背後說著話。吉克露出了驚訝的表情，但又很快就變成了別的表情。

「姆，多蘿瑟露姐姐，我還想嚇你一跳來著。」
「姆，多蘿瑟露姐姐，明明想嚇唬你的。」

一邊嘟噥惡作劇失敗了的事，一邊解開了隱藏身姿的魔術的蒂娜和迪特露出了他們的身姿。剛才和米蘭達蕾特說話的時候，蕾蒂雪爾感覺到了玄關大廳裡的魔素在移動。雖然蕾蒂雪爾並不能像精靈一樣感覺到所有魔素的流動，但是如果是在近處的話能感覺到。

「你們兩個怎麼了？」
「『我們來了！』」
「你們來了也⋯」

對著這兩個用閃閃的嚴管看著這邊的精靈王，蕾蒂雪爾看起來很為難。他們的目光說明了他們想玩。作為公主時代埋頭於戰鬥和研究的蕾蒂雪爾，看到天真爛漫的她們，無論如何都想照顧他們。

「我知道了，今天就做你們兩個想做的事。」
「『好耶！』」
「吉克怎麼辦？」
「我的話⋯」

蒂娜和迪特緊緊地抓住了沉默不語的吉克的衣襟下擺。

「『一起來玩啊。』」

編織同樣的言詞，同樣的臉的雙胞胎一起仰視著吉克。連那歪著頭的動作也完美地同步。

「他們是這麼說的。」
「⋯⋯」
「你討厭孩子嗎？」
「不是不是，只是，我是個無關的人⋯」
「這不就是個混熟的好機會嗎，蒂娜和迪特都迫不及待地想和你玩了。」

對於蕾蒂雪爾的話，緊貼著吉克的雙胞胎一起點頭。可能是因為昨天的事情吧，他們對吉克好像很感興趣。

「⋯那麼就請讓我一起。」

結果，吉克敗給了雙胞胎的抬眼攻擊。就在那一瞬間，蕾蒂雪爾清楚地地看到了蒂娜和迪特的眼睛就像調皮鬼一樣閃耀著。不管實際年齡如何，作為精靈來說他們還是孩子。看起來就像是看人類５歳的孩子一樣可愛。

「你們兩個想去哪兒玩呢？」
「『花園。』」

看來他們很中意昨天玩的花園啊。蕾蒂雪爾和吉克追上了從本館飛出去的漂漂然的雙子。因為到目的地並不是那麼遠，所以小跑一會兒就到了。蒂娜和迪特先到了，已經在花圃裡滾來滾去，做著花冠了。微笑地看著這個光景，蕾蒂雪爾也坐在雙胞胎的旁邊。說到能在花圃裡做的事，很遺憾，蕾蒂雪爾只能想到追逐嬉戲，還有躺下睡著，或是做個花冠。嘛，前世只活了１６年，也沒體驗過別的什麼玩的。

「花冠？那是什麼？」
「你不知道嗎？就像這樣把花和葉子一起編成這種環狀。」
「誒。」

吉克像是看到了稀罕事一樣，默默的看著蕾蒂雪爾編織花冠。看他那個反應總覺得像是個不懂世事的大少爺。本來以為這在平民間是個很普通的遊戲，果然是因為男孩子不喜歡這種遊戲嗎？

「多蘿瑟露姐姐，做好了。」
「多蘿瑟露姐姐，我做成了。」

蒂娜和迪特同時做好了花冠，明明昨天才第一次教他們的，沒想到做得出乎意料的工整。

「做的非常好看啊，我都覺得你們不是初學者了。」

被誇獎的兩人露出了得意的神情。兩人手牽著手在空中轉來轉去，把手裡拿著的花冠分別戴帶兩個人的頭上。蒂娜給蕾蒂希爾，迪特給了吉克。

「看啊，吉克哥哥你帶上了。」
「看啊，多蘿瑟露姐姐你帶上了。」

當初還是公主的時候，她也經常和喜歡的人一起編織花冠。雖然不是自己做的，但是蕾蒂雪爾有了懷念的心情。

「⋯⋯」

戴著花冠的吉克，他那非人的美貌和周圍花圃的景色相結合，綻放出虛幻而神聖的美麗。但是，他本人可能不習慣戴花冠，一直在把頭上的飾物調來調去，反覆地做出一些奇怪的行動。

「啊，吉克哥哥害羞了。」
「啊，吉克兄臉紅了。」

蒂娜面無表情，迪特則滿面笑容地戲弄著吉克。蕾蒂雪爾也不自覺得看著吉克的側臉，這樣是害羞了嗎？但是看起來和平時一樣啊。

「你們，不要說一些多餘的話啊。」
「哇⋯」

不知道是不是因為被戲弄而感到懊悔，吉克輕輕地站了起來，開始跑了起來。雙胞胎興高采烈地逃開了。看著在裡花圃精氣滿滿地開始追逐的雙胞胎和吉克（大概雙方都手下留情了），蕾蒂雪爾禁不住笑起來了。

「怎麼了，多蘿瑟露大小姐？」
「沒什麼，只是在想吉克意外地孩子氣呢。」

蕾蒂雪爾這樣說著，吉克一瞬間被嚇了一跳，但馬上露出了彆扭的表情。無表情女王蕾蒂雪爾想著，總是浮現出笑容的吉克，也會有這樣的表情啊。

「請不要管我。」
「哼哼」

對著耳朵微微發紅而轉向一邊的吉克，蕾蒂雪爾小聲地笑了。

在蕾蒂雪爾與雙胞胎精靈王一起玩的時候，奧茲瓦爾德在王城的維亞特里斯城堡的辦公室的桌子上陷入了沉思。他的那隻手裡握著一個金色吊墜。雖然美麗，但卻讓人覺得有些古老，一眼就能看得出來這個吊墜是被非常珍視著的。

「⋯⋯對不起，約瑟芬娜。」

那是第一王子的母親，奧茲瓦爾德一邊想著現在已經去世的最愛的王妃，一邊凝視著她最後的遺物。那個表情裡透露著悲傷和後悔。
叩叩
辦公室的門被輕輕地敲響了。把吊墜插入口袋後，奧茲瓦爾德發出了入內的許可。

「陛下，宰相大人請求覲見。」

走進房間的侍從向他報告到。

「知道了，讓他進來吧。」
「我知道了。」

侍從行了一個禮後就走了。等了一會兒，在侍從的帶領下，席利烏斯進入了辦公室。

「席利烏斯，親筆信帶到了嗎？」
「是的。雖然帶到了⋯」

看到席利烏斯臉上表情複雜，皺著眉頭，奧茲瓦爾德不由得察覺到是公爵家有什麼情況。

「看你那樣子，那個姑娘是不是拒絶了我的命令？」
「是的⋯」

關於在公爵家發生的事情，席利烏斯用書信記錄下來向奧茲瓦爾德報告。席利烏斯沒有勇氣直接向国王傳達多蘿瑟露的態度和話語。

「嗯姆⋯」
「違反国王的命令，真是無禮之極。要把她抓起來嗎？」
「不，那個女孩的說法事實，怨恨我也是正常的吧。為了表示誠意，我這就親自去公爵家吧。」
「三十，陛下⋯」
「不用再說了，馬上準備出發。」
「我知道了。」

在口袋裡悄悄地觸摸著隱藏著的吊墜，奧茲瓦爾德站了起來。

「哦，對了，蒂娜和迪特。」
「怎麼了？」
「你們真的想跟著到食堂來嗎？」

蕾蒂雪爾一邊走著，一邊問著老樣子手牽手的雙子。時間是午休前１０分鐘。為了吃午飯，蕾蒂雪爾一行人為了去食堂而在本館的走廊上走著。

「嗯，我對人類的點心有興趣。」
「嗯，我想嘗嘗人類的料理。」
「嘛，你們兩人想的話就沒關係。」
「好呀！」

看著手牽著手舉起雙手做出萬歳姿勢的雙胞胎精靈王，蕾蒂雪爾他們也做出了同樣的動作。現在，吉克和迪特、蕾蒂希爾和蒂娜各自牽著手並排成一列走著。

「我好像看到了食堂將陷入大混亂了。」
「反正昨天的事全部都暴露了。事到如今再怎麼在意也沒有用了。」
「說是這麼說啦⋯」
「有人說奇怪的話就幹掉他們。」
「有壊人的話就懲罰他。」
「別，還是別那樣吧。」

一邊說著這樣的對話，蕾蒂雪爾一行人走進了食堂。因為還在上課，食堂裡一個人也沒有。座位被吉克占著，蕾蒂雪爾帶著雙胞胎去了食堂的櫃檯。

「哇⋯⋯多蘿瑟露姐姐，這個草莓做的白色三角形的食物是什麼？」
「哦⋯⋯！多蘿瑟露姐姐，這個稍微鼓起的黃色雞蛋的料理是什麼？」

大概是第一次看到人間的食物吧，蒂娜和迪特，和當初剛入學時因與前世完全不同的現代的食物們而變得情緒高漲的蕾蒂雪爾是同樣的狀況。兩個人在煩惱著這樣不行，那個也不行的時候，午休開始的鈴聲響了，選完的時候食堂已經人山人海了。
回到座位上後，蕾蒂雪爾的朋友們已經聚集在一起。不⋯⋯只有一個人。

「維羅尼卡大人呢？」
「啊，維羅尼卡大人休息了！」

對蕾蒂雪爾的提問，與維羅尼卡同班的希爾梅斯回答到。可能他家是想到昨天的事而感覺不安吧。蕾蒂雪爾這樣想到。

「多蘿瑟露大小姐⋯⋯後面的那些孩子們⋯⋯」
「是的，正如你所想，是精靈。女孩是蒂娜，男孩是迪特。」
「『你好。』」

蕾蒂雪爾介紹了雙胞胎後，米蘭達雷特就呆滯了。

「米蘭，怎麼了？」
「可⋯」
「可？」
「真可愛！太可愛了吧！？有點像洋娃娃！我叫米蘭達雷特，你們就叫我米蘭姐姐吧！」
「米蘭姐姐？」
「米蘭姐。」
「是的是的！哇，太可愛了！」

米蘭達雷特就像換了個人，她帶著恍惚的表情顫抖不已。雙胞胎沒有注意到這股熱情小小的歪著頭，這讓他們看起來更萌了。

「⋯⋯」

對於平時沒見過的有人的這個狀態，蕾蒂希爾不知道該作何反應，結果選擇保持了面無表情。

「對不起，多蘿瑟露小姐。露露那傢伙，其實超喜歡小孩子的。」

希爾梅斯一邊撓頭一邊說。他估計到米蘭達雷特會是這個反應。

「是這樣的嗎？」
「嗯。我有一個年紀相差很遠的妹妹，露露露是末子卻比別人加倍地照顧我的妹妹。之前讓我妹妹叫她姐姐，她還流鼻血了。」
「這也太⋯」

真是非常喜歡孩子啊。那樣的話，看到眼前這散漫地笑著的米蘭達雷特也是可以理解的了。

「⋯⋯喂！那個是⋯⋯」
「在這種時候能來⋯⋯是發生了什麼嗎？」
「為什麼不是學生的弗里德大人⋯⋯？」

突然，食堂的嘈雜聲變大了。回頭看向入口，一個青年從擁擠的人群中向這邊走了過來。那個銀髮有著藤色瞳孔的青年，一看到蕾蒂雪爾，眼神就變了。
哎呀，好像在哪兒聽過弗里德這個人⋯

「喂，多蘿瑟露！回去了！」

走近的青年等不及地大聲喊著，抓住了蕾蒂希爾的手臂。蕾蒂雪爾不由得甩開了。

「有什麼事嗎？」
「才不是有什麼事，很不好了，總之一起回去。」
「不要。」
「国王陛下到我們家來了。」

聽了青年的話，蕾蒂希爾的朋友們都僵掉了。一臉不在乎樣子的只有蕾蒂雪爾和臉頰成塞得滿滿的的雙胞胎精靈。

（⋯⋯說起來，菲利亞里吉斯公爵家的嫡子確實是弗利德這個名字呢）

此刻，蕾蒂雪爾想起了因為存在感太過稀薄而完全忘記了的哥哥的存在，內心裡砰地一聲拍了下手。而且，国王比想像中還要早地行動了，這讓她很吃驚。雖然第一印象不是很好，但也許是個講道理的人。如果国王表示了他的誠意，這邊也應該放下身段來對話吧。
蕾蒂雪爾考慮起來了想要傳達給国王的事情。

「⋯⋯多蘿瑟露小姐，你又做了什麼嗎？」
「嘛，各種各樣的。」
「這樣啊⋯總之我明白了這是沒辦法的事情。」
「是聊天的時候嗎，好了，快回去。」
「你這樣拉我我不會回去的，請放開。」

剝下了哥哥的手後，蕾蒂雪爾對朋友們打了招呼，離開了那裡。

「⋯⋯？」

但是在離去的時候感覺到強烈的視線，回頭一看，元兇是希爾梅斯。希爾梅斯目不轉睛地看蕾蒂雪爾。他想說的話大概知道了，連續兩天的劍術練習都泡湯了。第一喜歡訂婚者，第二喜歡劍術的他，希望明天放學後不要爆發，懷著這樣的想法的蕾蒂雪爾和弗里德一起離開了學校。
回到公爵邸後，入口處的傭人全體出動迎接兩人。他們的代表，執事長的爺爺向前邁出了一步。

「歡迎回來，弗里德少爺，德魯塞爾大小姐⋯⋯⋯陛下已經在接待室等候了。」
「嗯，馬上就去。」

執事長與弗里德一邊交流著一邊消失在門的另一邊。蕾蒂雪爾也打算跟過去，但卻發現在慌慌張張地退散的傭人當中，有兩個人目不轉睛地看著這邊。

「你們兩人，在幹什麼？」

蕾蒂雪爾一打招呼，路維克和妮可兒就提心弔膽地跑到這邊來了。兩人都臉色慘白，不安的視線搖擺著。

「我，我們是在擔心大小姐。」
「真的沒關係嗎？昨天發生的事，在加上今天早上⋯」

即使只是一個表情、一句話，也能深深地傳達出他們的不安和擔心。說這種話的，在這個房子裡估計只有路維克和妮可兒了。

「不用擔心喲，謝謝你們兩人了。」
「⋯誒？」

看著路維克和妮可兒一臉傻氣的樣子，蕾蒂雪爾好像什麼都沒發生過一樣走過去了。被留在現場的兩個人相視而笑，臉上浮現出又驚訝又高興的複雜笑容。
另一方面，蕾蒂雪爾來到了接待室。
弗里德和執事長已經在等著了，對遲到的蕾蒂雪爾投以欲言又止的視線，一眨眼的工夫就又岔開了。執事長手扶著接待室的門，慢慢地推開了。
在會客室裡，一副無精打採的面孔的公爵夫婦，表情僵硬的席利烏斯，以及王国第４７代国王奧茲瓦爾德正等待著蕾蒂雪爾的到來。